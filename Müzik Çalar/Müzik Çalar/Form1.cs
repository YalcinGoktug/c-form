﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Müzik_Çalar
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.Text = "Bir şey çalmamakta...";
        }
        #region Seçme tuşu
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dosya = new OpenFileDialog();
            dosya.Filter = "Ses Dosyası |*.mp3;*.flac;*.vbs| Video|*.avi;*.mp4| Tüm Dosyalar |*.*";
            dosya.Title = "Dosya Seç";
            dosya.ShowDialog();
            string DosyaYolu = dosya.FileName;
            axWindowsMediaPlayer1.URL = DosyaYolu;
            label1.Text = axWindowsMediaPlayer1.currentMedia.name;
        }
        #endregion
        #region Oynatma Ayarları
        private void Form1_Load(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.uiMode = "none";
        }
        #region Ses ayarları
        private void button3_Click(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.settings.volume-=10;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.settings.volume+=10;
        }
        private void button6_Click(object sender, EventArgs e)
        {
            if (axWindowsMediaPlayer1.settings.mute == true)
                axWindowsMediaPlayer1.settings.mute = false;
            else
                axWindowsMediaPlayer1.settings.mute = true;
        }
        #endregion
        private void button4_Click(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.Ctlcontrols.play();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.Ctlcontrols.pause();
        }
        private void CheckEnter(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                MessageBox.Show("ASDFSAFAS");
            }
        }
        #endregion

        

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            Random randomizer = new Random();

            int r = randomizer.Next(0, 255), g = randomizer.Next(0, 255), b = randomizer.Next(0, 255);

            this.BackColor = Color.FromArgb(r, g, b);
            this.label1.BackColor = Color.FromArgb(r, g, b);
        }

        private void axWindowsMediaPlayer1_MouseMoveEvent(object sender, AxWMPLib._WMPOCXEvents_MouseMoveEvent e)
        {
            Random randomizer = new Random();

            int r = randomizer.Next(0, 255), g = randomizer.Next(0, 255), b = randomizer.Next(0, 255);

            this.BackColor = Color.FromArgb(r, g, b);
            this.label1.BackColor = Color.FromArgb(r, g, b);
        }
    }
}
