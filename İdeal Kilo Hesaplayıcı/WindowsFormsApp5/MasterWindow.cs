﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    class MasterWindow : Form
    {
        public MasterWindow(int w, int y)
        {
            InitializeComponent();
            #region Ana İskelet
            this.BackColor = System.Drawing.Color.CadetBlue;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vücut Kitle Endeksi";
            Width = w;
            Height = y;

            TextBox t = new TextBox();
            Label tLabel = new Label();
            tLabel.Text = "Boyunuz : ";
            tLabel.SetBounds(47, 50, 59, 29);
            t.SetBounds(50, 70, 50, 80);
            Controls.Add(t);
            Controls.Add(tLabel);

            TextBox l = new TextBox();
            Label lLabel = new Label();
            lLabel.Text = "Kilonuz : ";
            lLabel.SetBounds(147, 50, 59, 29);
            l.SetBounds(150, 70, 50, 80);
            Controls.Add(l);
            Controls.Add(lLabel);

            Button b = new Button();
            b.Text = "Hesapla";
            b.SetBounds(90, 120, 70, 22);
            Controls.Add(b);
            b.BackColor = System.Drawing.Color.White;

            Label sonuc = new Label();
            sonuc.Text = "";
            sonuc.SetBounds(65, 160, 170, 30);

            RadioButton kadin = new RadioButton();
            RadioButton erkek = new RadioButton();
            kadin.Text = "Kadın";
            erkek.Text = "Erkek";
            if (kadin.Checked == true)
                erkek.Checked = false;
            if (erkek.Checked == true)
                kadin.Checked = false;
            erkek.SetBounds(150, -20, 90, 90);
            kadin.SetBounds(50, -20, 90, 90);
            Controls.Add(erkek);
            Controls.Add(kadin);

            Label fark = new Label();
            fark.Text = "";
            fark.SetBounds(65, 200, 200, 230);

            b.Click += B_Click;

            void B_Click(object sender, EventArgs e)
            {
                if (kadin.Checked == false && erkek.Checked == false)
                {
                    MessageBox.Show("Cinsiyet Seçmelisiniz !", "Hata",MessageBoxButtons.OK,MessageBoxIcon.Hand);
                    Environment.Exit(1);
                }
                double son = 0.0;
                if (kadin.Checked)
                {
                    son = 45.5 + 2.3 * ((Double.Parse(t.Text) * 0.39) - 60);
                }
                if (erkek.Checked)
                {
                    son = 50 + 2.3 * ((Double.Parse(t.Text) * 0.39) - 60);
                }
                sonuc.Text = "İdeal Kilonuz : " + son.ToString().Substring(0, 5);
                double kilo = son - Int32.Parse(l.Text);
                if (son < 0)
                    fark.Text = "İdeal Kilonuzdan " + kilo.ToString().Substring(0, kilo.ToString().IndexOf(',')) + " kilo almalısınız.";
                if (son > 0)
                    fark.Text = "İdeal Kilonuzdan " + (kilo*-1).ToString().Substring(0, (kilo.ToString().IndexOf(',')-1)) + " kilo vermelisiniz.";
                if (son == 0)
                    fark.Text = "İdeal Kilonuzdasınız !";
                Controls.Add(sonuc);
                Controls.Add(fark);
            }
                #endregion
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MasterWindow));
            this.SuspendLayout();
            // 
            // MasterWindow
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MasterWindow";
            this.ResumeLayout(false);

        }
    }
}

