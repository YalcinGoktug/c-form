// Author(s) : Ali Goktug Yalcin (for now)

// Reason for doing those : Beginning to C# WRF Forms and Re-write apps which I use in daily life.

// Number of projects : 3


//			   TR - EN
// Hesap Makinesi - Calculator
	Günlük hesaplamalar için kullanılabilecek basit bir hesap makinesi.
	An easy calculator for daily calculations.
	
// Medya Çalar - Media Player
	Windows Media Player 11 temelli bir medya çalıcı. Henüz geliştirme sürecinde olsa da
	müzik, video oynatabiliyor; resim ve fotoğrafları gösterebiliyor.
	A media player, based on WMP11. in dev phase already but It can play videos and music files;
	shows photos and pictures.
	
// İdeal Kilo Hesaplayıcı - Ideal Weight Calculator
	Şu an ki kilonuz ve boyunuz ile cinsiyetinize göre ideal kilo hesabınızı yaparak
	vermeniz veya almanız gereken kiloyu hesaplar.
	Give your weight, height and gender and see how you far from your ideal weight.