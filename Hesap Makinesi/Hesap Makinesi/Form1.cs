﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hesap_Makinesi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            if (textBox1.Text == "0")
            {
                textBox1.Text = "1";
            }
            else
            {
                textBox1.Text = textBox1.Text + "1";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            z = " ";
        }
        private void Clear_Click(object sender, EventArgs e)
        {
            x = 0;
            y = 0;
            z = "";
            textBox1.Text = "0";
        }
        private void button1_Click(object sender, EventArgs e) //1 sayısının butonu
        {
            if (textBox1.Text == "0")
            {
                textBox1.Text = "1";
            }
            else
            {
                textBox1.Text = textBox1.Text + "1";
            }
        }
        private void button2_Click(object sender, EventArgs e) //2 sayısının butonu
        {
            if (textBox1.Text == "0")
            {
                textBox1.Text = "2";
            }
            else
            {
                textBox1.Text = textBox1.Text + "2";
            }
        }
        private void button3_Click(object sender, EventArgs e) //3 sayısının butonu
        {
            if (textBox1.Text == "0")
            {
                textBox1.Text = "3";
            }
            else
            {
                textBox1.Text = textBox1.Text + "3";
            }
        }
        private void button4_Click(object sender, EventArgs e) //4 sayısının butonu
        {
            if (textBox1.Text == "0")
            {
                textBox1.Text = "4";
            }
            else
            {
                textBox1.Text = textBox1.Text + "4";
            }
        }
        private void button5_Click(object sender, EventArgs e) //5 sayısının butonu
        {
            if (textBox1.Text == "0")
            {
                textBox1.Text = "5";
            }
            else
            {
                textBox1.Text = textBox1.Text + "5";
            }
        }
        private void button6_Click(object sender, EventArgs e) //6 sayısının butonu
        {
            if (textBox1.Text == "0")
            {
                textBox1.Text = "6";
            }
            else
            {
                textBox1.Text = textBox1.Text + "6";
            }
        }
        private void button7_Click(object sender, EventArgs e) //7 sayısının butonu
        {
            if (textBox1.Text == "0")
            {
                textBox1.Text = "7";
            }
            else
            {
                textBox1.Text = textBox1.Text + "7";
            }
        }
        private void button8_Click(object sender, EventArgs e) //8 sayısının butonu
        {
            if (textBox1.Text == "0")
            {
                textBox1.Text = "8";
            }
            else
            {
                textBox1.Text = textBox1.Text + "8";
            }
        }
        private void button9_Click(object sender, EventArgs e) //9 sayısının butonu
        {
            if (textBox1.Text == "0")
            {
                textBox1.Text = "9";
            }
            else
            {
                textBox1.Text = textBox1.Text + "9";
            }
        }
        private void button10_Click(object sender, EventArgs e) //0 sayısının butonu
        {
            if (textBox1.Text == "0")
            {
                textBox1.Text = "0";
            }
            else
            {
                textBox1.Text = textBox1.Text + "0";
            }
        }
        private void button11_Click(object sender, EventArgs e) //Toplama butonu
        {
            x = Convert.ToDouble(textBox1.Text);
            z = "+";
            textBox1.Text = " ";
        }
        private void button12_Click(object sender, EventArgs e) //Çıkarma butonu
        {
            x = Convert.ToDouble(textBox1.Text);
            z = "-";
            textBox1.Text = " ";
        }
        private void button13_Click(object sender, EventArgs e) //Çarpma butonu
        {
            x = Convert.ToDouble(textBox1.Text);
            z = "*";
            textBox1.Text = " ";
        }
        private void button14_Click(object sender, EventArgs e) //Bölme butonu
        {
            x = Convert.ToDouble(textBox1.Text);
            z = "/";
            textBox1.Text = " ";
        }
        private void button15_Click(object sender, EventArgs e) //Eşittir butonu
        {
            y = Convert.ToDouble(textBox1.Text);
            if (z == "+")
            {
                textBox1.Text = Convert.ToString(x + y);
            }
            if (z == "-")
            {
                textBox1.Text = Convert.ToString(x - y);
            }
            if (z == "*")
            {
                textBox1.Text = Convert.ToString(x * y);
            }
            if (z == "/")
            {
                textBox1.Text = Convert.ToString(x / y);
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Process.Start("https://bitbucket.org/YalcinGoktug/");
        }
    }
}